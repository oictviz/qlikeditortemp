# Web Mashup Template with Qlik Mashup Editor

QlikSense provide you with an easy-to-use editor to do web mashup for your qlik app.

## About this repository
The source of this repository is a simply mashup example using [the UN Datacatalog Visualization app](https://viz.unite.un.org/visualization/single?appid=0e886feb-9668-42e5-a439-eb1b471a61b7&sheet=DrwBFm&opt=nointeraction&select=clearall). You can download it by clicking the `Download` at the left menu and test the connection with your CMS (Content Management System).

## Useful documentation
- [Preparing your mashup deployment](https://help.qlik.com/en-US/sense-developer/1.1/Subsystems/Workbench/Content/BuildingWebsites/HowTos/deploy-mashup-preparations.htm)
- [Display a Web page on a SharePoint page by adding the Page Viewer Web Part](https://support.office.com/en-us/article/Display-a-Web-page-on-a-SharePoint-page-by-adding-the-Page-Viewer-Web-Part-7f61feec-9b3d-4805-a960-07636ba59527)

## How to create a template with Qlik Mashup Editor

- Open Qlik Desktop
- Go to Dev-Hub

    ![Capture.PNG](https://bitbucket.org/repo/dk6n4R/images/4258941323-Capture.PNG)

- Click Mashup editor -> Create New Project -> Choose Grid Mashup template -> Create
- Arrange each objects as you want with bootstrap
- After this, go to C:\Users\<username>\Documents\Qlik\Sense\Extensions\<yourmashupname>
- Change the name of HTML file to index

## How to use the template to connect with a app hosted on QlikSense Server
- Change the head of HTML file, the configuration and App Id as [HERE](https://bitbucket.org/oictviz/public-wiki/wiki/Mashup%20Configuration)

## How to test check the result on your local desktop
  - Use [BabyWebServer (Windows)](http://www.pablosoftwaresolutions.com/html/baby_web_server.html) or [MAMP (MAC)](https://www.mamp.info/en/) when testing the website on your local desktop, because other ways, such as SimpleHTTPServer, cannot have your website connect to the qlik server